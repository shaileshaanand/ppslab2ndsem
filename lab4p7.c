/* WAP to print factorial of a number using function 
 * without return type */
#include<stdio.h>
void factorial(int,int*);
int main() {
    int n,fact;
    printf("Enter a number\n");
    scanf("%d",&n);
    factorial(n,&fact);
    printf("Factorial=%d\n",fact);
    return 0;
}
void factorial(int num,int *fact) {
    int i;
    *fact=1;
    for (i=2;i<=num;i++) {
        *fact*=i;
    }
}

