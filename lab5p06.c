/* WAP to find 1-3+5-7+9+11... */
#include<stdio.h>
int main() {
    int i,n,ft,sum=0;
    printf("Enter number of terms:\n");
    scanf("%d",&n);
    ft=2*n-1;
    for (i=1;i<=ft;i+=2) {
        if ((i+1)%4==0) {
            sum-=i;
        } else {
            sum+=i;
        }
    }
    printf("Sum=%d\n",sum);
    return 0;
}
