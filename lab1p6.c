/* WAP to enter 3 numbers and print the biggest of three numbers using simple
 * if else */
#include<stdio.h>
int main() {
    int a,b,c;
    printf("Enter 3 numbers: ");
    scanf("%d %d %d",&a,&b,&c);
    if (a>=b && a>=c) {
        printf("%d is biggest.\n",a);
    } else if (b>=a && b>=c) {
        printf("%d is biggest.\n",b);
    } else if (c>=a && c>=b) {
        printf("%d is biggest.\n",c);
    }
    return 0;
}
