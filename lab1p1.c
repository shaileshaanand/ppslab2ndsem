/* WAP to enter Student's roll number and 3 subjects and print total and
 * average*/
#include<stdio.h>
int main() {
    int roll,m1,m2,m3,total;
    double avg;
    printf("Enter your roll no.:");
    scanf("%d",&roll);
    printf("Enter your marks in 3 subjects:");
    scanf("%d %d %d",&m1,&m2,&m3);
    total=m1+m2+m3;
    avg=total/3.0;
    printf("Roll no.:%d\n",roll);
    printf("Total=%d\n",total);
    printf("Average=%f\n",avg);
    return 0;
}
