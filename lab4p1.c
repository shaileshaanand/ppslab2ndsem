/* bubble sort using function */
#include<stdio.h>
void bubble_sort(int num[],int len);
int main() {
    int i,l=10,nums[10];
    printf("Enter %d numbers:\n",l);
    for (i=0;i<l;i++) {
        scanf("%d",&nums[i]);
    }
    bubble_sort(nums,l);
    printf("\n\nSorted Array:\n");
    for (i=0;i<l;i++) {
        printf("%d\n",nums[i]);
    }
    return 0;
}
void bubble_sort(int num[],int len) {
    int i,j,temp;
    for (i=0;i<len;i++) {
        for (j=0;j<len-i;j++) {
            if (num[j]>num[j+1]) {
                temp=num[j];
                num[j]=num[j+1];
                num[j+1]=temp;
            }
        }
    }
}
