/* WAP to find 2-4+6-8+10.... */
#include<stdio.h>
int main() {
    int i,n,ft,sum=0;
    printf("Enter number of terms:\n");
    scanf("%d",&n);
    ft=2*n;
    for (i=2;i<=ft;i+=2) {
        if (i%4==0) {
            sum-=i;
        } else {
            sum+=i;
        }
    }
    printf("Sum=%d\n",sum);
    return 0;
}
