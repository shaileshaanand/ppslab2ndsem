/* WAP to calculate the area of different shapes according to user input
 * for right angled triangle, rectangle, circle. */
#include<stdio.h>
int main() {
    int choice,a,b;
    printf("Choices:\n1-Right angled triangle\n2-Rectangle\n3-Circle\n");
    scanf("%d",&choice);
    switch (choice) {
        case 1:
            printf("Enter base and height:");
            scanf("%d %d",&a,&b);
            printf("Area = %f\n",0.5*a*b);
            break;
        case 2:
            printf("Enter length and breadth:");
            scanf("%d %d",&a,&b);
            printf("Area = %f\n",(double)a*b);
            break;
        case 3:
            printf("Enter radius:");
            scanf("%d",&a);
            printf("Area = %f\n",3.14*a*a);
            break;
    }
    return 0;
}
