/* WAP to calculate 1+(1/2)+(1/3)+ ...... */
#include<stdio.h>
int main() {
    int i,n;
    double sum=0.0;
    printf("Enter no. of terms\n");
    scanf("%d",&n);
    for (i=1;i<=n;i++) {
        sum+=1.0/i;
    }
    printf("Sum=%f\n",sum);
    return 0;
}
