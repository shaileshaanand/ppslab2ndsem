/* WAP to enter a number and check whethera number is armstrong or not */
#include<stdio.h>
#include<math.h>
int main() {
    int num,sum=0,temp,digits=0,d;
    printf("Enter a number:");
    scanf("%d",&num);
    temp=num;
    while (temp!=0) {
        digits++;
        temp/=10;
    }
    temp=num;
    while (temp!=0) {
        d=temp%10;
        sum+=(int)pow(d,digits);
        temp/=10;
    }
    if (num==sum) {
        printf("%d is Armstrong.\n",num);
    } else {
        printf("%d is not Armstrong.\n",num);
    }
    return 0;
}

