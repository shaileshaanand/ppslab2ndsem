/* WAP to print the transpose of a given matrix */
#include<stdio.h>
int main() {
    int i,j,dim,temp;
    printf("Enter dimension:\n");
    scanf("%d",&dim);
    int a[dim][dim];
    printf("Enter a %dX%d matrix\n",dim,dim);
    for (i=0;i<dim;i++) {
        for (j=0;j<dim;j++) {
            scanf("%d",&a[i][j]);
        }
    }
    for (i=0;i<(dim-1);i++) {
        for (j=i+1;j<dim;j++) {
            temp=a[i][j];
            a[i][j]=a[j][i];
            a[j][i]=temp;
        }
    }
    printf("\nTranspose of the matrix is:\n");
    for (i=0;i<dim;i++) {
        for (j=0;j<dim;j++) {
            printf("%d ",a[i][j]);
        }
        printf("\n");
    }
    return 0;
}
