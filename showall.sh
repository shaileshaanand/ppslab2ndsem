#!/bin/bash
regex="*.c"
for file in *
do
    if [[ ${file} =~ lab[0-9]p[0-9]{1,}.c ]]
    then
        printf "\n\n\n------------------------------${file}------------------------------\n"
        cat $file
    fi
done
