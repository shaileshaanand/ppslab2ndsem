/* WAP to check whether a given number is palindrome or not */
#include<stdio.h>
#include<string.h>
int main() {
    int flag=1,i,l;
    char num[255];
    printf("Enter a number: ");
    scanf("%s",&num);
    l=strlen(num);
    for (i=0;i<l/2;i++) {
        if (num[i]!=num[l-i-1]) {
            flag=0;
            break;
        }
    }
    if (flag) {
        printf("Palindrome\n");
    } else {
        printf("Not Palindrome\n");
    }
    return 0;
}
