/* Find the total marks, grade. percentage of a student that appeared in 5 
 * different subjects and grade according the percentage */
/* Percentage     Grade
 *  >=80            E+
 *  70-80           E
 *  60-70           A+
 *  50-60           A
 *  40-50           B
 *  <40             fail
 */
#include<stdio.h>
int main() {
    int m1,m2,m3,m4,m5,total,i;
    double per;
    printf("Enter marks in 5 subjects:");
    scanf("%d %d %d %d %d",&m1,&m2,&m3,&m4,&m5);
    total=m1+m2+m3+m4+m5;
    per=total/5.0;

    printf("Total Marks = %d\n",total);
    printf("Percentage = %f\n",per);
    if (per>=80.0) {
        printf("Grade:E+\n");
    } else if (per>=70) {
        printf("Grade:E\n");
    } else if (per>=60) {
        printf("Grade:A+\n");
    } else if (per>=50) {
        printf("Grade:A\n");
    } else if (per>=40) {
        printf("Grade:B\n");
    } else {
        printf("Grade:fail\n");
    }
    return 0;
}
