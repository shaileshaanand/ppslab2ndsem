/* WAP to print prime numbers from 1 to 100 */
#include<stdio.h>
#include<math.h>
int main() {
    int num,j,flag;
    for (num=2;num<=100;num++) {
        flag=1;
        for (j=2;j<=sqrt(num);j++) {
            if (num%j==0) {
                flag=0;
                break;
            }
        }
        if (flag) {
            printf("%d, ",num);
        }
    }
    return 0;
}
