/* WAP to check weather a given number is prime or not*/
#include<stdio.h>
#include<math.h>
int isPrime(int);
int main() {
    int num;
    printf("Enter a number\n");
    scanf("%d",&num);
    if (isPrime(num)) {
        printf("%d is prime\n",num);
    } else {
        printf("%d is not prime\n",num);
    }
    return 0;
}
int isPrime(int num) {
    int i,rt=sqrt(num);
    for (i=2;i<=rt;i++) {
        if (num%i==0) {
            return 0;
        }
    }
    return 1;
}


