/* WAP to swap 2 numbers without using third variable */
#include<stdio.h>
int main() {
    int a,b;
    printf("Enter 2 numbers:");
    scanf("%d %d",&a,&b);
    printf("Before:\na=%d\nb=%d\n\n",a,b);
    a=a+b;
    b=a-b;
    a=a-b;
    printf("After:\na=%d\nb=%d\n",a,b);
    return 0;
}
