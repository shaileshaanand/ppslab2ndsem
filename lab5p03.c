/* WAP to find sum of (1 X 2 X 3)+(2 X 3 X 4)... */
#include<stdio.h>
int main() {
    int i,n;
    double sum=0.0;
    printf("Enter no. of terms\n");
    scanf("%d",&n);
    for (i=1;i<=n;i++) {
        sum+=i*(i+1)*(i+2);
    }
    printf("Sum=%f\n",sum);
    return 0;
}
