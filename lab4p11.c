/* WAP to display the elements of an array whose
 * access of elements can be done using pointer
 * of the array */
#include<stdio.h>
int main() {
    int arr[]={5,7,4,8,1,74,86,4,78,11};
    int i,*ptr=arr;
    for (i=0;i<10;i++) {
        printf("%d\n",(*(ptr+i)));
    }
    return 0;
}
