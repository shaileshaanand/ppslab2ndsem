/* WAP to swap 2 numbers using third variable */
#include<stdio.h>
int main() {
    int a,b,t;
    printf("Enter 2 numbers:");
    scanf("%d %d",&a,&b);
    printf("Before:\na=%d\nb=%d\n\n",a,b);
    t=a;
    a=b;
    b=t;
    printf("After:\na=%d\nb=%d\n",a,b);
    return 0;
}
