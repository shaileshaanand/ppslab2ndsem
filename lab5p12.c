/* WAP to enter a 2D array and find the sum of
 * each row and each column separately */
#include<stdio.h>
int main() {
    int i,j,sumr,sumc,a[3][3];
    printf("Enter a 3X3 matrix:\n");
    for (i=0;i<3;i++) {
        scanf("%d %d %d",&a[i][0],&a[i][1],&a[i][2]);
    }
    for (i=0;i<3;i++) {
        sumr=0;
        sumc=0;
        for (j=0;j<3;j++) {
            sumr+=a[i][j];
            sumc+=a[j][i];
        }
        printf("Sum of Row %d=%d\n",(i+1),sumr);
        printf("Sum of Column %d=%d\n",(i+1),sumc);
    }
    return 0;
}
