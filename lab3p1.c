/* WAP to print Fibonacci Series and its sum */
#include<stdio.h>
int main() {
    int a=1,b=1,sum=1,i,temp,terms;
    printf("Enter number of terms:");
    scanf("%d",&terms);
    printf("%d, ",a);
    for (i=2;i<=terms;i++) {
        temp=a;
        a=b;
        b=temp+b;
        printf("%d, ",a);
        sum+=a;
    }
    printf("\nSum=%d\n",sum);
    return 0;
}

