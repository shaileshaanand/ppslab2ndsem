/* WAP to find sum of (1/1!)+(2/2!)+(3/3!)+......*/
#include<stdio.h>
int fact(int);
int main() {
    int i,n;
    double sum=0.0;
    printf("Enter no. of terms\n");
    scanf("%d",&n);
    for (i=1;i<=n;i++) {
        sum+=(double)i/fact(i);
    }
    printf("Sum=%f\n",sum);
    return 0;
}
int fact(int num) {
    int i,fact=1;
    for (i=2;i<=num;i++) {
        fact*=i;
    }
    return fact;
}
