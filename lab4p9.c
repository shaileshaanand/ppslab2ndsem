/* WAP to display the address of a variable using
 * pointer variable */
#include<stdio.h>
int main() {
    int a=10,*p;
    p=&a;
    printf("%p\n",p);
    return 0;
}

