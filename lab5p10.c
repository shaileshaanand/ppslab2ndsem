/* WAP to accept 2 matrices and perform multiplication */
#include<stdio.h>
int main() {
    int i,j;
    int a[3][3],b[3][3],c[3][3];
    printf("Enter first 3X3 matrix:\n");
    for (i=0;i<3;i++) {
        scanf("%d %d %d",&a[i][0],&a[i][1],&a[i][2]);
    }
    printf("Enter second 3X3 matrix:\n");
    for (i=0;i<3;i++) {
        scanf("%d %d %d",&b[i][0],&b[i][1],&b[i][2]);
    }
    for (i=0;i<3;i++) {
        for (j=0;j<3;j++) {
            c[i][j]=a[i][0]*b[0][j]+a[i][1]*b[1][j]+a[i][2]*b[2][j];
        }
    }
    printf("\nProduct of matrices is:\n");
    for (i=0;i<3;i++) {
        for (j=0;j<3;j++) {
            printf("%d ",c[i][j]);
        }
        printf("\n");
    }
    return 0;
}
