/* Write a switch statement that will examine the value of char-type variable
 * color and print one of the following messages
 *
 * RED     for 'r' or 'R'
 * GREEN   for 'g' or 'G'
 * BLUE    for 'b' or 'B'
 * BLACK   for any other char.
 */
#include<stdio.h>
int main() {
    char color;
    printf("Enter color as char:");
    scanf("%c",&color);
    switch (color) {
        case 'r':
        case 'R':
            printf("RED\n");
            break;
        case 'g':
        case 'G':
            printf("GREEN\n");
            break;
        case 'b':
        case 'B':
            printf("BLUE\n");
            break;
        default:
            printf("BLACK\n");
    }
    return 0;
}
