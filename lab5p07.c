/* WAP to count the number of vowels in a given 
 * character array */
#include<stdio.h>
#include<string.h>
int main() {
    int i,count=0,l;
    char str[255];
    printf("Enter a string:\n");
    gets(str);
    l=strlen(str);
    for (i=0;i<l;i++) {
        if (str[i]=='a' || str[i]=='e' || str[i]=='i' || str[i]=='0' || str[i]=='u' || str[i]=='A' || str[i]=='E' || str[i]=='I' || str[i]=='O' || str[i]=='U') {
            count++;
        }
    }
    printf("No. of vowels=%d\n",count);
    return 0;
}
