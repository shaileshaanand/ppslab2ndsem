/* WAP to print sum of individual digits of a number */
#include<stdio.h>
int main() {
    int num,sum=0;
    printf("Enter a number: ");
    scanf("%d",&num);
    while (num!=0) {
        sum+=num%10;
        num/=10;
    }
    printf("Sum = %d\n",sum);
    return 0;
}
