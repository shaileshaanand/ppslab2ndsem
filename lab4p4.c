/* Wap to swap 2 nos. by call by value */
#include<stdio.h>
void swap(int,int);
int main() {
    int a,b;
    printf("Enter 2 nunbers\n");
    scanf("%d %d",&a,&b);
    printf("\nBefore:\n");
    printf("a=%d, b=%d\n",a,b);
    swap(a,b);
    return 0;
}
void swap(int a,int b) {
    int temp;
    temp=a;
    a=b;
    b=temp;
    printf("\nAfter:\n");
    printf("a=%d, b=%d\n",a,b);
}
