/* WAP a program to take 10 input no. from user and 
 * copy all odd elements into other array and print
 * them using function */
#include<stdio.h>
void copyodd(int num[],int odd[]);
int main() {
    int l,num[10],odd[],i;
    printf("Enter 10 numbers:\n");
    for (i=0;i<9;i++) {
        scanf("%d",&num[i]);
    }
    copyodd(num,odd);
    l=sizeof(odd)/sizeof(int);
    for (i=0;i<l;i++) {
        printf("%d\n",odd[i]);
    }
    return 0;
}
void copyodd(int num[],int odd[]) {
    int size=0,i;
    for (i=0;i<10;i++) {
        if (num[i]%2==1) {
            odd[size]=num[i];
            size++;
        }
    }
}
