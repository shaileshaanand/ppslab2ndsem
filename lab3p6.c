/* WAP to enter choice for summation, subtraction, multiplication, division */
#include<stdio.h>
int main() {
    char choice;
    int n1,n2,result;
    printf("Enter choice + - / *\n");
    scanf("%c",&choice);
    printf("Enter 2 numbers\n");
    scanf("%d %d",&n1,&n2);
    switch (choice) {
        case '+':
            printf("Sum=%d\n",n1+n2);
            break;
        case '-':
            printf("Difference=%d\n",n1-n2);
            break;
        case '*':
            printf("Product=%d\n",n1*n2);
            break;
        case '/':
            printf("Division=%f\n",((double)n1)/n2);
            break;
    }
    return 0;
}
